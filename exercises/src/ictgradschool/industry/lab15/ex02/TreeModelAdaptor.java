package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.*;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by ycow194 on 15/05/2017.
 */
public class TreeModelAdaptor implements TreeModel {

    private NestingShape _adaptee;

    public TreeModelAdaptor(NestingShape _adaptee) {
        this._adaptee = _adaptee;
    }

    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        NestingShape castedShape = (NestingShape) parent;

        return castedShape.shapeAt(index);
    }

    @Override
    public int getChildCount(Object parent) {
        NestingShape castedShape = (NestingShape) parent;


        return castedShape.shapeCount();
    }

    @Override
    public boolean isLeaf(Object node) {

        if (node instanceof NestingShape) {
            return false;
        }
        return true;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        NestingShape castedShape = (NestingShape) parent;
        Shape castedChild = (Shape) child;


        return castedShape.indexOf(castedChild);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }

//    public int getChildCount(Shape parent){
//        int result = 0;
//        Shape shape = (Shape) parent;
//
//        if (shape instanceof NestingShape){
//            NestingShape nestingShape = (NestingShape) shape;
//            result = nestingShape.getNumberOfChildren();
//        }
//        return result;
//    }
//
//    public Shape getChild(NestingShape parent, int index) {
//        Shape result = null;
//
//        if (parent instanceof Shape){
//            Shape sha = (Shape) parent;
//
//        }
//        return result;
//    }
//
//    public int getIndexOfChild(NestingShape parent, Shape child) {
//
//
//    }
}
