package ictgradschool.industry.lab15.ex01;

import sun.reflect.generics.tree.Tree;
import sun.security.provider.SHA;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ycow194 on 15/05/2017.
 */
public class NestingShape extends Shape {

    private List<Shape> shapeList = new ArrayList<>();


    public NestingShape() {
        shapeList = new ArrayList<>();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }


    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (int i = 0; i < shapeList.size(); i++) {
            shapeList.get(i).move(this.getWidth(), this.getHeight());

        }

    }

    public void paint(Painter painter) {
        // Draws a one off rectangle

        painter.drawRect(getX(), getY(), getWidth(), getHeight());

        painter.translate(this.getX(), this.getY());
        // Draws children inside arraylist
        for (int i = 0; i < shapeList.size(); i++) {
            shapeList.get(i).paint(painter);
        }
        painter.translate(-this.getX(),-this.getY());

    }

    public void add(Shape child) throws IllegalArgumentException {

        for (int i = 0; i < shapeList.size(); i++) {
            if (child.equals(shapeList.get(i))) {
                throw new IllegalArgumentException("Your shape already exists inside the NestingShape object");
            }
        }

        shapeList.add(child);
        child.setParent(this);

    }

    public void remove(Shape child) {
        shapeList.remove(child);
        child.setParent(null);
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {

        if (index < 0 || index > (shapeList.size() - 1)) {
            throw new IndexOutOfBoundsException("index of the shape is out of bounds");
        }

        return shapeList.get(index);

    }

    public int shapeCount() {
        return shapeList.size();
    }

    public int indexOf(Shape child) {

        return shapeList.indexOf(child);

    }

    public boolean contains(Shape child) {
        for (int i = 0; i < shapeList.size(); i++) {
            if (child.equals(shapeList.get(i))) {
                return true;
            }
        }
        return false;
    }
}
